package main

import (
	"fmt"
	"database/sql"
	"time"
)

type ass_questions_hdr struct {
	Question_id int `json:"question_id"`
	Question_name string `json:"question_name"`
	Question_description string `json:"question_description"`
	Question_category string `json:"question_category"`
	Created_date string  `json:"created_date"`
	Created_by int `json:"created_by"`
	Updated_date string `json:"updated_date"`
	Updated_by int `json:"updated_by"`
}

type ass_questions_dtl struct {

	questions_answer_id int `json:"questions_answer_id"`
	questions_id int `json:"questions_id"`
	questions_description string `json:"questions_description"`
	question_flag_correct string `json:"question_flag_correct"`
	Created_date *time.Time  `json:"created_date"`
	Created_by int `json:"created_by"`
	Updated_date *time.Time  `json:"updated_date"`
	Updated_by int `json:"updated_by"`
}

type ass_participants struct{
	Participants_id int `participants_id`
	User_id int `user_id`
	Participants_name string `participants_name`
	Participants_last_name string `participants_last_name`
	Participants_address string `participants_address`
	Participants_birth_date string `participants_birth_date`
	Created_date string  `json:"created_date"`
	Created_by int `json:"created_by"`
	Updated_date string `json:"updated_date"`
	Updated_by int `json:"updated_by"`
}

func (qh *ass_questions_hdr) createQuesitionHdr(db *sql.DB) error {
	//t := time.Now()
	statement := fmt.Sprintf("INSERT INTO ass_questions_hdr" +
		"(question_name, question_description, question_category, created_date, created_by, updated_date, updated_by) " +
		"VALUES('%s','%s','%s',now(),'%d',now(), %d)", qh.Question_name, qh.Question_description,qh.Question_category,-1,-1)
	_, err := db.Exec(statement)

	if err != nil {
		return err
	}

	err = db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&qh.Question_id)

	if err != nil {
		return err
	}

	return nil
}

func getQuestions(db *sql.DB, start, count int) ([]ass_questions_hdr, error) {
	statement := fmt.Sprintf("SELECT question_name, question_description, question_category, created_date, created_by, updated_date, updated_by FROM ass_questions_hdr LIMIT %d OFFSET %d", count, start)
	rows, err := db.Query(statement)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	ass_questions_hdrs := []ass_questions_hdr{}

	for rows.Next() {
		var qh ass_questions_hdr
		if err := rows.Scan(&qh.Question_name, &qh.Question_description, &qh.Question_category,&qh.Created_date,&qh.Created_by,&qh.Updated_date,&qh.Updated_by); err != nil {
			return nil, err
		}
		ass_questions_hdrs = append(ass_questions_hdrs, qh)
	}

	return ass_questions_hdrs, nil
}

func getParticipant(db *sql.DB, start, count int) ([]ass_participants, error) {
	statement := fmt.Sprintf("SELECT Participants_name, Participants_last_name, Participants_address, created_date, created_by, updated_date, updated_by FROM ass_participants LIMIT %d OFFSET %d", count, start)
	rows, err := db.Query(statement)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	ass_participant := []ass_participants{}

	for rows.Next() {
		var ap ass_participants
		if err := rows.Scan(&ap.Participants_name, &ap.Participants_last_name, &ap.Participants_address,&ap.Created_date,&ap.Created_by,&ap.Updated_date,&ap.Updated_by); err != nil {
			return nil, err
		}
		ass_participant = append(ass_participant, qh)
	}

	return ass_participant, nil
}