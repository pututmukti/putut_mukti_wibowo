-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 08, 2018 at 05:06 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assessments`
--

-- --------------------------------------------------------

--
-- Table structure for table `ass_participants`
--

CREATE TABLE `ass_participants` (
  `participants_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `participants_name` varchar(50) NOT NULL,
  `participants_last_name` varchar(50) NOT NULL,
  `participants_address` varchar(100) NOT NULL,
  `participants_birth_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ass_questions_answer`
--

CREATE TABLE `ass_questions_answer` (
  `questions_answer_id` int(11) NOT NULL,
  `questions_id` int(11) NOT NULL,
  `questions_description` varchar(1000) NOT NULL,
  `question_flag_correct` tinyint(1) NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ass_questions_hdr`
--

CREATE TABLE `ass_questions_hdr` (
  `question_id` int(11) NOT NULL,
  `question_name` varchar(200) NOT NULL,
  `question_description` text NOT NULL,
  `question_category` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ass_questions_hdr`
--

INSERT INTO `ass_questions_hdr` (`question_id`, `question_name`, `question_description`, `question_category`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(3, 'Soal SBMPTN', 'Membran sel bersifat impermeabel', 'Biologi', '2018-05-08 08:15:33', -1, '2018-05-08 08:15:33', -1),
(4, 'Soal SBMPTN', 'Sel fagosit berperan penting dalam', 'Biologi', '2018-05-08 08:16:46', -1, '2018-05-08 08:16:46', -1),
(5, 'Soal SBMPTN', 'Jaringan penguat pada tumbuhan dikotil yang sel-sel penyusunnya masih hidup adalah…', 'Biologi', '2018-05-08 08:17:05', -1, '2018-05-08 08:17:05', -1),
(6, 'Soal SBMPTN', 'Hewan bersel satu berikut yang dapat digolongkan ke dalam satu kelas dengan Euglena adalah…', 'Biologi', '2018-05-08 08:17:25', -1, '2018-05-08 08:17:25', -1),
(7, 'Soal SBMPTN', 'Pada respirasi aerob, oksigen berperan pada proses…', 'Biologi', '2018-05-08 08:17:47', -1, '2018-05-08 08:17:47', -1),
(8, 'Soal SBMPTN', 'Kemungkinan butawarna pada anak perempuan hasil perkawinan antara istri butawarna dan suami normal adalah…', 'Biologi', '2018-05-08 08:18:06', -1, '2018-05-08 08:18:06', -1),
(9, 'Soal SBMPTN', 'Hewan bersel satu berikut yang dapat digolongkan ke dalam satu kelas dengan Euglena…', 'Biologi', '2018-05-08 09:17:47', -1, '2018-05-08 09:17:47', -1);

-- --------------------------------------------------------

--
-- Table structure for table `ass_trn_answer_dtl`
--

CREATE TABLE `ass_trn_answer_dtl` (
  `trn_answer_dtl_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `questions_answer_id` int(11) NOT NULL,
  `trn_answer_hdr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ass_trn_answer_hdr`
--

CREATE TABLE `ass_trn_answer_hdr` (
  `trn_answer_hdr_id` int(11) NOT NULL,
  `participants_id` int(11) NOT NULL,
  `total_not_answered` int(11) NOT NULL,
  `total_right_answer` int(11) NOT NULL,
  `total_wrong_answer` int(11) NOT NULL,
  `total_score` int(11) NOT NULL,
  `test_start` datetime NOT NULL,
  `test_end` datetime NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ass_users`
--

CREATE TABLE `ass_users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_token_login` varchar(200) NOT NULL,
  `user_token_created` time NOT NULL,
  `user_login_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ass_participants`
--
ALTER TABLE `ass_participants`
  ADD PRIMARY KEY (`participants_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `ass_questions_answer`
--
ALTER TABLE `ass_questions_answer`
  ADD PRIMARY KEY (`questions_answer_id`),
  ADD KEY `questions_id` (`questions_id`);

--
-- Indexes for table `ass_questions_hdr`
--
ALTER TABLE `ass_questions_hdr`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `ass_users`
--
ALTER TABLE `ass_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ass_participants`
--
ALTER TABLE `ass_participants`
  MODIFY `participants_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ass_questions_answer`
--
ALTER TABLE `ass_questions_answer`
  MODIFY `questions_answer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ass_questions_hdr`
--
ALTER TABLE `ass_questions_hdr`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ass_users`
--
ALTER TABLE `ass_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ass_participants`
--
ALTER TABLE `ass_participants`
  ADD CONSTRAINT `ass_participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ass_users` (`user_id`);

--
-- Constraints for table `ass_questions_answer`
--
ALTER TABLE `ass_questions_answer`
  ADD CONSTRAINT `ass_questions_answer_ibfk_1` FOREIGN KEY (`questions_id`) REFERENCES `ass_questions_hdr` (`question_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
